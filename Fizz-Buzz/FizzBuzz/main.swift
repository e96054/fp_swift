import Foundation

let fizz = { i in i % 3 == 0 ? "fizz" :  ""}
//func fizz(_ i : Int) -> String {
//    if i % 3 == 0 {
//        return "fizz"
//    }
//    return ""
//}

let buzz = { i in i % 5 == 0 ? "buzz" :  ""}
//func buzz(_ i : Int) -> String {
//    if i % 5 == 0 {
//        return "buzz"
//    }
//    return ""
//}
let fizzbuzz : (Int) -> String = { i in { a, b in b.isEmpty ? a : b}("\(i)", fizz(i) + buzz(i))}


func loop(min: Int, max: Int, do f:(Int)-> Void) {
    Array(min...max).forEach(f)
}

loop(min: 1, max: 100, do: { print(fizzbuzz($0)) })



