import Foundation

enum Result : String {
    case wrong = "Wrong"
    case correct = "Correct!"
    case high = "High"
    case low = "Low"
}

func generateAnswer(_ min:Int, _ max:Int) -> Int {
    return Int(arc4random()) % (max - min) + min
}

func inputAndCheck(_ answer: Int) -> () -> Bool {
    return { printResult(evaluateInput(answer)) }
}

func evaluateInput(_ answer: Int) -> Result {
    guard let inputNumber = Int(readLine() ?? "") else { return .wrong }
    if inputNumber > answer { return .high}
    if inputNumber < answer { return .low}
    return .correct
}

func printResult(_ r:Result) -> Bool {
    if case .correct = r { return false }
    print(r.rawValue)
    return true
}



func corrected(_ count:Int) -> Void {
    print("Correct! : \(count)")
}



func countingLoop(_ needContinue: @escaping () -> Bool, _ finished: (Int) -> Void){
    
    func counter(_ c:Int) -> Int {
        if !needContinue() { return c}
        return counter(c + 1)
    }
    finished(counter(0))
}

countingLoop(inputAndCheck(generateAnswer(1, 100)), corrected)
